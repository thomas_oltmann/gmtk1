
Colors = {
	Normal: ["#EEEEEE", "#2573BD", "#F65912"],
	Light: ["#EEEEEE", "#4795DF", "#FF7B34"],
	// Light: ["#EEEEEE", "#EEEEEE", "#EEEEEE"],
	Dark: ["#AAAAAA", "#02408A", "#C32600"],
	Edge: "#AAAAAA"
};

WantedFPS = 60;
Canvas = null;
TileSize = 5;
MinWorldDist = TileSize * 30;
MaxWorldDist = TileSize * 50;
FleetSpeed = 30 / 60;
UnitRadius = 6;
ProductionSpeed = 3 / 60;

InGame = 0;
Lost = 1;
Won = 2;

Worlds = [];
Edges = [];
Fleets = [];
Selection = null;
Neighbors = [];
Classes = [];
Ai = null;
State = 0;

Balancing = [
	[0.50, 0.35, 0.65],
	[0.65, 0.50, 0.35],
	[0.35, 0.65, 0.50]];

// Corvette (Triangle)
Classes.push({
	idx: 0,
	clip: function(ctx) {
		ctx.beginPath();
		ctx.moveTo(0, -UnitRadius);
		ctx.lineTo(-UnitRadius, UnitRadius);
		ctx.lineTo(UnitRadius, UnitRadius);
		ctx.lineTo(0, -UnitRadius);
		ctx.clip();
	}
});

// Frigate (Square)
Classes.push({
	idx: 1,
	clip: function(ctx) {
		ctx.beginPath();
		ctx.rect(-UnitRadius, -UnitRadius, UnitRadius * 2, UnitRadius * 2);
		ctx.clip();
	}
});

// Bomber (Circle)
Classes.push({
	idx: 2,
	clip: function(ctx) {
		ctx.beginPath();
		ctx.arc(0, 0, UnitRadius, 0, Math.PI * 2, false);
		ctx.clip();
	}
});



// inclusive low, exclusive high
function randomBetween(low, high) {
	return low + Math.floor(Math.random() * (high - low));
}

function distance2d(a, b) {
	let x = a.x - b.x;
	let y = a.y - b.y;
	return Math.sqrt(x*x + y*y);
}

Array.prototype.extend = function(other_array) {
    other_array.forEach(function(v) {this.push(v)}, this);    
}



function World(size, x, y) {
	this.x = x;
	this.y = y;
	this.size = size;
	this.radius = this.size * TileSize / 2;
	this.visible = false;
	this.player = 0;
	this.produces = Classes[randomBetween(0, Classes.length)];
	this.progress = 0.0;
	this.maxResources = Math.floor(this.size / 2);
	this.resources = Math.floor(this.size / 2);
	this.numWorkers = 0;
	this.units = [];
	let numUnits = randomBetween(0, Math.floor(this.size / 2));
	for (let i = 0; i < numUnits; i++) {
		this.units.push(this.produces);
	}
}

World.prototype.draw = function(ctx) {
	if (!this.visible) return;
	
	ctx.save();
	ctx.translate(this.x, this.y);

	ctx.fillStyle = Colors.Normal[this.player];

	ctx.beginPath();
	ctx.arc(0, 0, this.radius, 0, Math.PI * 2, false);
	ctx.fill();

	let startAngle = (Date.now() / 1000 / 6 + this.x) % (Math.PI * 2);
	let angle = Math.PI * 2 / this.units.length;
	for (let i = 0; i < this.units.length; i++) {
		ctx.save();
		ctx.translate(Math.sin(startAngle + angle * i) * this.radius * 1.5,
			Math.cos(startAngle + angle * i) * this.radius * 1.5);
		this.units[i].clip(ctx);
		ctx.fillRect(-UnitRadius, -UnitRadius, UnitRadius * 2, UnitRadius * 2);
		ctx.restore();
	}

	ctx.save();
	ctx.scale(1.5, 1.5);
	this.produces.clip(ctx);
	ctx.fillStyle = "#111111";
	ctx.fillRect(-UnitRadius, -UnitRadius, UnitRadius * 2, UnitRadius * 2);
	ctx.fillStyle = Colors.Dark[this.player];
	ctx.fillRect(-UnitRadius, UnitRadius - UnitRadius * 2 * this.resources / this.maxResources,
		UnitRadius * 2, UnitRadius * 2);
	ctx.restore();

	ctx.strokeStyle = "#EEEEEE";
	ctx.beginPath();
	ctx.arc(0, 0, this.radius, Math.PI * -0.5, Math.PI * 2 * this.progress / 100.0 + Math.PI * -0.5, false);
	ctx.stroke();

	ctx.restore();
};

World.prototype.update = function() {
	if (this.resources > 0) {
		this.progress += ProductionSpeed * this.numWorkers;
		if (this.progress >= 100.0) {
			this.progress -= 100.0;
			this.resources--;
			this.units.push(this.produces);
		}
	}
};

World.prototype.capture = function(player) {
	this.player = player;
	repairVisibility();
};

function repairVisibility() {
	let noHuman = true;
	let noAi = true;
	Ai.mine = [];
	Ai.neighbors = [];
	for (let w = 0; w < Worlds.length; w++) {
		Worlds[w].visible = false;
		if (Worlds[w].player == 1) {
			Worlds[w].visible = true;
			noHuman = false;
		}
		if (Worlds[w].player == 2) {
			Ai.mine.push(Worlds[w]);
			noAi = false;
		}
	}
	for (let e = 0; e < Edges.length; e++) {
		if (Edges[e].a.player == 1 && Edges[e].b.player != 1)
			Edges[e].b.visible = true;
		if (Edges[e].b.player == 1 && Edges[e].a.player != 1)
			Edges[e].a.visible = true;
		if (Edges[e].a.player == 2 && Edges[e].b.player != 2)
			Ai.neighbors.push(Edges[e].b);
		if (Edges[e].b.player == 2 && Edges[e].a.player != 2)
			Ai.neighbors.push(Edges[e].a);
	}
	if (noHuman) State = Lost;
	if (noAi) State = Won;
}


function Edge(a, b) {
	this.a = a;
	this.b = b;
	this.dist = distance2d(a, b);
	this.fleets = [];
}

Edge.prototype.draw = function(ctx) {
	if (this.a.player != 1 && this.b.player != 1) return;
	ctx.lineWidth = 2;
	ctx.strokeStyle = Colors.Edge;
	ctx.beginPath();
	ctx.moveTo(this.a.x, this.a.y);
	ctx.lineTo(this.b.x, this.b.y);
	ctx.stroke();
};



function Fleet(player, units, pos, dest) {
	this.player = player;
	this.units = units;
	this.x = pos.x;
	this.y = pos.y;
	this.dest = dest;
	let dist = distance2d(pos, dest);
	this.dir = {x: (dest.x - pos.x) / dist, y: (dest.y - pos.y) / dist};
	this.offsets = [];
	for (let u = 0; u < this.units.length; u++) {
		this.offsets.push({x: randomBetween(-20, 21), y: randomBetween(-20, 21)});
	}
}

Fleet.prototype.attack = function() {
	let enemies = this.dest.units;
	while (true) {
		if (enemies.length == 0) {
			this.dest.capture(this.player);
			this.dest.units.extend(this.units);
			this.remove();
			return;
		}
		if (this.units.length == 0) {
			this.remove();
			return;
		}
		let fleetIdx = randomBetween(0, this.units.length);
		let enemyIdx = randomBetween(0, enemies.length);
		let fleetUnit = this.units[fleetIdx];
		let enemyUnit = enemies[enemyIdx];
		let prob = Balancing[fleetUnit.idx][enemyUnit.idx];
		if (Math.random() < prob) {
			enemies[enemyIdx] = enemies[0];
			enemies.shift();
		} else {
			this.units[fleetIdx] = this.units[0];
			this.units.shift();
		}
	}
}

Fleet.prototype.update = function() {
	this.x += this.dir.x * FleetSpeed;
	this.y += this.dir.y * FleetSpeed;
	if (distance2d(this, this.dest) <= this.dest.radius * 1.5) {
		if (this.dest.player == this.player) {
			this.dest.capture(this.player);
			this.dest.units.extend(this.units);
			this.remove();
		} else {
			this.attack();
		}
	}
};

Fleet.prototype.draw = function(ctx) {
	if (!this.dest.visible) return;
	for (let u = 0; u < this.units.length; u++) {
		ctx.save();
		ctx.translate(this.x + this.offsets[u].x, this.y + this.offsets[u].y);
		this.units[u].clip(ctx);
		ctx.fillStyle = Colors.Normal[this.player];
		ctx.fillRect(-8, -8, 16, 16);
		ctx.restore();
	}
};

Fleet.prototype.remove = function() {
	Fleets[Fleets.indexOf(this)] = Fleets[0];
	Fleets.shift();
};



function AI() {
	this.cooldown = 0;
	this.mine = [];
	this.neighbors = [];
	this.curMine = 0;
}

function findHighest(array) {
	let highest = 0;
	for (let i = 0; i < array.length; i++) {
		if (array[i] > array[highest]) {
			highest = i;
		}
	}
	return highest;
}

AI.prototype.shouldSend = function(src, dest) {
	if (dest.player == 2) return true;
	let myNums = [0, 0, 0];
	for (let u = 0; u < src.units.length; u++)
		myNums[src.units[u].idx]++;
	let destNums = [0, 0, 0];
	for (let u = 0; u < dest.units.length; u++)
		destNums[dest.units[u].idx]++;
	let myMaj = findHighest(myNums);
	let destMaj = findHighest(destNums);
	return Balancing[myMaj][destMaj] * myNums[myMaj] >= Balancing[destMaj][myMaj] * destNums[destMaj];
};

AI.prototype.update = function() {
	this.cooldown--;
	if (this.cooldown <= 0) {
		this.cooldown += 30;
		if (this.mine.length == 0) return;
		this.curMine = (this.curMine + 1) % this.mine.length;
		let world = this.mine[this.curMine];
		let wantedWorkers = Math.round(world.resources / 4);
		while (world.numWorkers < wantedWorkers && world.units.length > 0) {
			world.units.shift();
			world.numWorkers++;
		}
		if (Math.random() >= 0.5) return;
		let exits = [];
		for (let e = 0; e < Edges.length; e++) {
			if (Edges[e].a == world) exits.push(Edges[e].b);
			if (Edges[e].b == world) exits.push(Edges[e].a);
		}
		let maxTries = randomBetween(1, 4);
		for (let t = 0; t < maxTries; t++) {
			let dest = exits[randomBetween(0, exits.length)];
			if (!this.shouldSend(world, dest)) continue;
			send(world, dest);
		}
	}
};



function generateBoard(size) {
	let homeWorlds = {dist: 0, a: null, b: null};

	for (let i = 0; i < size * size / 10; i++) {
		let world = new World(randomBetween(5, 16),
			randomBetween(-size, size + 1),
			randomBetween(-size, size + 1));
		let edges = [];
		if (distance2d(world, {x: 0, y: 0}) > size) {
			continue;
		}
		let opposite = {dist: 0, world: null};
		for (let j = 0; j < Worlds.length; j++) {
			let dist = distance2d(world, Worlds[j]);
			if (dist < MinWorldDist) {
				world = null;
				break;
			}
			if (dist <= MaxWorldDist) {
				edges.push(new Edge(world, Worlds[j]));
			}
			if (dist > opposite.dist) {
				opposite = {dist: dist, world: Worlds[j]};
			}
		}
		if (world == null) continue;
		if (opposite.dist > homeWorlds.dist) {
			homeWorlds = {dist: opposite.dist, a: world, b: opposite.world};
		}
		Worlds.push(world);
		Edges.extend(edges);
	}

	homeWorlds.a.capture(1);
	homeWorlds.a.numWorkers++;
	homeWorlds.b.capture(2);
	homeWorlds.b.numWorkers++;
}

function setup() {
	Ai = new AI();
	generateBoard(Canvas.height / 2 - 50);
	State = InGame;
}

function update() {
	for (let w = 0; w < Worlds.length; w++) {
		Worlds[w].update();
	}
	for (let f = 0; f < Fleets.length; f++) {
		Fleets[f].update();
	}
	Ai.update();
}

function draw() {
	let ctx = Canvas.getContext('2d');
	ctx.save();
	ctx.clearRect(0, 0, Canvas.width, Canvas.height);
	ctx.translate(Canvas.width / 2, Canvas.height / 2);

	for (let e = 0; e < Edges.length; e++) {
		Edges[e].draw(ctx);
	}
	for (let w = 0; w < Worlds.length; w++) {
		Worlds[w].draw(ctx);
	}
	for (let f = 0; f < Fleets.length; f++) {
		Fleets[f].draw(ctx);
	}

	if (Selection != null) {
		function outline(x, y, radius, color) {
			ctx.save();
			ctx.translate(x, y);
			ctx.lineWidth = 4;
			ctx.strokeStyle = color;
			ctx.beginPath();
			ctx.arc(0, 0, radius, 0, Math.PI * 2, false);
			ctx.stroke();
			ctx.restore();
		}
		outline(Selection.x, Selection.y, Selection.radius, "#BB5500");
		for (let n = 0; n < Neighbors.length; n++) {
			outline(Neighbors[n].x, Neighbors[n].y, Neighbors[n].radius, "#DDDD22");
		}
	}

	ctx.font = "12px Arial";
	ctx.fillStyle = Colors.Edge;
	ctx.fillText("(C) Thomas Oltmann, 2017", Canvas.width / 2 - 200, Canvas.height / 2 - 40);

	if (State === Lost) {
		ctx.font = "30px Arial";
		ctx.fillStyle = Colors.Edge;
		ctx.fillText("You Lost!", 0, 0);
	} else if (State === Won) {
		ctx.font = "30px Arial";
		ctx.fillStyle = Colors.Edge;
		ctx.fillText("You Won!", 0, 0);
	}

	ctx.restore();
}

function doFrame() {
	update();
	draw();
}

function onClick(event) {
	let point = {x: event.clientX - Canvas.width / 2, y: event.clientY - Canvas.height / 2};
	if (Selection != null && distance2d(point, Selection) <= UnitRadius * 1.5 && Selection.player == 1 && Selection.units.length > 0 && Selection.resources > 0) {
		Selection.units.shift();
		Selection.numWorkers++;
		return;
	}
	for (let w = 0; w < Worlds.length; w++) {
		let dist = distance2d(Worlds[w], point);
		if (dist < Worlds[w].radius) {
			if (Neighbors.indexOf(Worlds[w]) != -1) {
				send(Selection, Worlds[w]);
				Selection = null;
				Neighbors = [];
			} else if (Worlds[w].visible) {
				Selection = Worlds[w];
				Neighbors = [];
				if (Selection.player == 1) {
					for (let e = 0; e < Edges.length; e++) {
						if (Edges[e].a == Selection) Neighbors.push(Edges[e].b);
						else if (Edges[e].b == Selection) Neighbors.push(Edges[e].a);
					}
				}
			}
			return;
		}
	}
	Selection = null;
	Neighbors = [];
}

function send(src, dest) {
	if (src.units.length == 0) return;
	let fleet = new Fleet(src.player, src.units, src, dest);
	src.units = [];
	Fleets.push(fleet);
}

function doStart(event) {
	Canvas.removeEventListener("click", doStart);
	Canvas.addEventListener("click", onClick, false);
	setup();
	setInterval(doFrame, 1000 / WantedFPS);
}

function init() {
	Canvas = document.getElementById("gameCanvas");
	Canvas.width = Canvas.offsetWidth;
	Canvas.height = Canvas.offsetHeight;
	let ctx = Canvas.getContext('2d');
	ctx.save();
	ctx.translate(Canvas.width / 2, Canvas.height / 2);
	ctx.font = "30px Arial";
	ctx.fillStyle = Colors.Edge;
	ctx.fillText("Click to Start", 0, 0);
	ctx.restore();
	Canvas.addEventListener("click", doStart);
}

