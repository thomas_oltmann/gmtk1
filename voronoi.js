Canvas = null;
points = [];

function euclidean(a, b) {
	let x = Math.abs(a.x - b.x);
	let y = Math.abs(a.y - b.y);
	return Math.sqrt(x*x + y*y);
}

function manhattan(a, b) {
	let x = Math.abs(a.x - b.x);
	let y = Math.abs(a.y - b.y);
	return x + y;
}

function chebyshev(a, b) {
	let x = Math.abs(a.x - b.x);
	let y = Math.abs(a.y - b.y);
	return Math.max(x, y);
}

function mindim(a, b) {
	let x = Math.abs(a.x - b.x);
	let y = Math.abs(a.y - b.y);
	return Math.min(x, y);
}

function randomColor() {
	let r = Math.floor(Math.random() * 16) * 16;
	let g = Math.floor(Math.random() * 16) * 16;
	let b = Math.floor(Math.random() * 16) * 16;
	return "rgb(".concat(r, ", ", g, ", ", b, ")");
}

function nearest(x, y) {
	let q = {x: x, y: y};
	let min = Infinity, point = null;
	for (let p = 0; p < points.length; p++) {
		let dist = distFunc(q, points[p]);
		if (dist < min) {
			min = dist;
			point = points[p];
		}
	}
	return point;
}

function redraw() {
	let ctx = Canvas.getContext('2d');
	ctx.clearRect(0, 0, Canvas.width, Canvas.height);
	
	for (let x = 0; x < Canvas.width; x++) {
		for (let y = 0; y < Canvas.height; y++) {
			let n = nearest(x, y, manhattan);
			ctx.fillStyle = n.color;
			ctx.fillRect(x, y, 1, 1);
		}
	}
}

function onClick(event) {
	points.push({x: event.clientX, y: event.clientY, color: randomColor()});
	redraw();
}

function onKey(event) {
    if (event.keyCode == 69) {
		distFunc = euclidean;
		redraw();
    }
    else if (event.keyCode == 77) {
		distFunc = manhattan;
		redraw();
    }
	else if (event.keyCode == 67) {
		distFunc = chebyshev;
		redraw();
	}
	else if (event.keyCode == 78) {
		distFunc = mindim;
		redraw();
	}
}

function init() {
	distFunc = euclidean;
	Canvas = document.getElementById("mainCanvas");
	Canvas.width = Canvas.offsetWidth;
	Canvas.height = Canvas.offsetHeight;
	if (Canvas.getContext) {
		Canvas.addEventListener("click", onClick, false);
		document.addEventListener('keyup', onKey);
	}
}

